def shellSort(a):
    gap = len(a)//2
    while gap > 0:
        for start in range(gap):
            insertSort(a, start, gap)
        gap = gap // 2
    return a


def insertSort(a, start, gap):
    for i in range(start+gap, len(a), gap):
        current = a[i]
        pos = i
        while pos >= gap and a[pos-gap]>current:
            a[pos] = a[pos-gap]
            pos = pos-gap
        a[pos] = current


a = [1,4,7,34,765,23,46,23,12,98]
print(a)
print(shellSort(a))
