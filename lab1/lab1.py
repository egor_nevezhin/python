__author__ = 'Egor Nevezhin'
import math
import doctest


def is_prime(n):
    """
    This function return true if number is prime and false in otherwise.

    >>> is_prime(7)
    True
    >>> is_prime(11)
    True
    >>> is_prime(4)
    False
    """
    for i in range(2, n-1):
        if n % i == 0:
            return False
    return True


def is_happy(n):
    """
    This function return true if number is happy and false in otherwise.

    >>> is_happy(19)
    True
    >>> is_happy(4)
    False
    """
    n = str(n)
    tmp = list()
    while (n != 1 and n not in tmp):
        tmp.append(n)
        n = sum(int(i)**2 for i in str(n))
        if(n == 4):
            return False
    return True

def is_triangular(n):
    """
    This function return true if number is triangular and false in otherwise.

    >>> is_triangular(0)
    True
    >>> is_triangular(3)
    True
    >>> is_triangular(120)
    True
    >>> is_triangular(7)
    False
    """
    i = 1
    while(n > 0):
        n = n - i
        i += 1
    if(n == 0):
        return True
    else:
        return False


def is_square(n):
    """
    This function return true if number is square and false in otherwise.

    >>> is_square(1)
    True
    >>> is_square(4)
    True
    >>> is_square(121)
    True
    >>> is_square(35)
    False
    """
    i = 1
    while(n > 0):
        n = n - i
        i+=2
    if(n == 0):
        return True
    else:
        return False


def is_smug(n):
    """
    This function return true if number is smug and false in otherwise.

    >>> is_smug(5)
    True
    >>> is_smug(13)
    True
    >>> is_smug(7)
    False
    """
    for i in range(1, n):
        for j in range(i, n):
            if(i**2 + j**2 == n):
                return True
    return False


def is_honest(n):
    """
    This function return true if number is honest and false in otherwise.

    >>> is_honest(1)
    True
    >>> is_honest(3)
    True
    >>> is_honest(10)
    False
    >>> is_honest(17)
    False
    """
    for i in range(1, int(math.sqrt(n)+1)):
        if(i**2 != n) and (n // i == i):
            return False
    return True


a = 0
while(a <= 0):
    print("Please, write a positive number")
    a = input()
    if(a.isdigit()):
        a = int(a)


    doctest.testmod(verbose=True)


if(is_prime(a)):
    print("The number is prime")
else:
    print("The number is not prime")

if(is_happy(a)):
    print("The number is happy")
else:
    print("The number is not happy")

if(is_triangular(a)):
    print("The number is triangular")
else:
    print("The number is not triangular")

if(is_square(a)):
    print("The number is square")
else:
    print("The number is not square")

if(is_smug(a)):
    print("The number is smug")
else:
    print("The number is not smug")

if(is_honest(a)):
    print("The number is honest")
else:
    print("The number is not honest")