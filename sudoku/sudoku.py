def read_sudoku(filename):
    ''' Прочитать Судоку из указанного файла '''
    digits = [c for c in open(filename).read() if c in '123456789.']
    grid = group(digits, 9)
    return grid


def display(values):
    ''' Вывести на экран пазл, состоящий из значений values '''
    width = 2
    line = '+'.join(['-' * (width * 3)] * 3)
    for row in range(9):
        print(''.join(values[row][col].center(width) + \
            ('|' if str(col) in '25' else '') for col in range(9)))
        if str(row) in '25':
            print(line)
    print()

def group(values, n):
	return [values[0+i*n:n+i*n] for i in range(n)]


def get_row(values, pos):
	row, col = pos
	return [values[row][i] for i in range(len(values))]


def get_col(values, pos):
	row, col = pos
	return [values[i][col] for i in range(len(values))]


def get_block(values, pos):
	row, col = pos
	return [values[i][j] for i in range(row//3*3, row//3*3+3) for j in range(col//3*3, col//3*3+3)]


def find_empty_position(values):
	for i in range(len(values)):
		for j in range(len(values)):
			if(values[i][j] == '.'):
				return (i, j)


def find_possible_values(grid, pos):
	r = []
	for i in range(1, 10):
		if((str(i) not in get_block(grid, pos)) and (str(i) not in get_col(grid, pos)) and (str(i) not in get_row(grid, pos))):
			r.append(str(i))
	return r



def solve(values):
	pos = find_empty_position(values)
	if not pos:
		s = True
		return values #solved

	value = find_possible_values(values, pos)
	if not value:
		return None
	for i in value:
		values[ pos[0] ][ pos[1] ] = str(i)
		solution = solve(values)
		if not solution:
			values[ pos[0] ][ pos[1] ] = '.'
		else:
			return solution


def check_solution(solution):
	for i in range(0, 9):
		for j in range(0, 9):
			row = get_row(solution, (i, j))
			col = get_col(solution, (i, j))
			block = get_block(solution, (i, j))
			for k in range(1, 10):
				if((str(k) not in row) or (str(k) not in col) or (str(k) not in block)):
					return False
	return True

grid = read_sudoku('puzzle1.txt')
display(grid)

sol = solve(grid)
if(sol != None):
	display(sol)
	if(check_solution(sol)):
		print("Solution is correct.")
	else:
		print("Solution isn't correct.")
else:
	print("Sudoku haven't solution.")





