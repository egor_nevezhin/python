def shellSort(a):
    gap = len(a)//2
    while gap > 0:
        for start in range(gap):
            insertSort(a, start, gap)
        gap = gap // 2
    return a


def insertSort(a, start, gap):
    for i in range(start+gap, len(a), gap):
        current = a[i]
        pos = i
        while pos >= gap and a[pos-gap]>current:
            a[pos] = a[pos-gap]
            pos = pos-gap
        a[pos] = current



def mergeSort(a):
    if(len(a) > 1):
        middle = len(a)//2
        left = a[:middle]
        right = a[middle:]
        mergeSort(left)
        mergeSort(right)

        i = 0
        j = 0
        k = 0

        while i<len(left) and j<len(right):
            if(left[i]<right[j]):
                a[k] = left[i]
                i+=1
            else:
                a[k] = right[j]
                j+=1
            k+=1

        while (i < len(left)):
            a[k] = left[i]
            i+=1
            k+=1

        while (j < len(right)):
            a[k] = right[j]
            j+=1
            k+=1
    #return a


def generateRandomNumbers(n, start=0, stop=100):
    import random
    return [random.randint(start, stop) for r in range(n)]


def plotTimeComplexity(sortAlgorithm1, sortAlgorithm2, max_size):
    import random
    import time
    import matplotlib.pyplot as plt
    
    time1 = []
    size1 = []
    time2 = []
    size2 = []
    
    for size in range(1000, max_size+90000, 400):
        vals = generateRandomNumbers(size)

        start_time = time.time()
        sortAlgorithm1(vals)
        stop_time = time.time()
        time1.append(stop_time - start_time)
        size1.append(size)

        start_time = time.time()
        sortAlgorithm2(vals)
        stop_time = time.time()
        time2.append(stop_time - start_time)
        size2.append(size)

        print(size)
    
    plt.plot(size1, time1, 'ro', size2, time2, 'bo')
    plt.show()



plotTimeComplexity(shellSort, mergeSort, 10000);